// Bài 1: Tìm số nguyên dương nhỏ nhất sao cho 1 + 2 + .. + n > 10000
document.getElementById("btn-so-nguyen-duong").onclick = function () {
  // input: number
  var num = 10000;
  // output: number
  var soNguyenDuongNhoNhat = 0;
  // xử lý
  // khởi tạo vòng lặp
  // tạo biến chứa tổng
  var sum = 0;
  for (var i = 1; i < num; i++) {
    sum = sum + i;
    if (sum > num) {
      soNguyenDuongNhoNhat = i;
      break;
    }
  }
  //   show kết quả lên giao diện
  document.getElementById("result1").innerHTML =
    "Số nguyên dương nhỏ nhất thỏa điều kiện = " + soNguyenDuongNhoNhat;
};

// // Bài 2:Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2
// + x^3 + … + x^n
document.getElementById("btn-tinh-tong").onclick = function () {
  // input: number
  var x = document.getElementById("txt-so-thuc").value * 1;
  var n = document.getElementById("txt-so-mu").value * 1;
  // output: number
  var sum = 0;
  //   tính lũy thừa
  var luyThua = 0;
  luyThua = Math.pow(x, n);
  // khởi tạo vòng lặp
  for (var i = 1; i <= x; i++) {
    sum = x + luyThua;
  }
  // show kết quả
  document.getElementById("result2").innerHTML = "Tổng = " + sum;
};

// Bài 3: Nhập vào số n. Tính giai thừa 1 * 2 * ... * n
document.getElementById("btn-giai-thua").onclick = function () {
  // input: number
  var n = document.getElementById("txt-so-n").value * 1;
  // output: number
  var giaiThua = 1;
  // khởi tạo vòng lặp
  for (var i = 1; i <= n; i++) {
    giaiThua = giaiThua * i;
  }
  // show kết quả
  document.getElementById("result3").innerHTML = "Giai thừa = " + giaiThua;
};

// Bài 4: Tạo thẻ div
document.getElementById("taoThe").onclick = function () {
  // output: string
  var output = "";
  // khởi tạo vòng lặp
  for (var i = 1; i <= 10; i++) {
    if (i % 2 === 0) {
      output =
        output +
        "<div class='bg-danger text-white p-2 mb-2 text-center'>DIV CHẴN</div>";
    } else {
      output =
        output +
        "<div class='bg-success text-white p-2 mb-2 text-center'>DIV LẺ</div>";
    }
  }
  //   show kêt quả
  document.getElementById("result4").innerHTML = output;
};
